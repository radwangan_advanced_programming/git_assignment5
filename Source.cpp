#include <iostream>
#include <string.h>
#include <fstream>
#include "Helper.h"
#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#pragma comment(lib, "User32.lib")
#include <vector>
using namespace std;
typedef unsigned int(WINAPI* AvVersion)(void);

int main()
{
	Helper help;
	string st;
	vector<string> vec;
	TCHAR pwd[MAX_PATH];
	while (1)
	{
		cout << ">> ";
		getline(cin,st);
		//help.trim(st);
		vec = help.get_words(st);
		if (vec[0] == "pwd")
		{
			if (!GetCurrentDirectory(MAX_PATH, pwd))
			{
				std::cerr << "Error getting current directory: #" << GetLastError();
				return 1; // quit if it failed
			}
			cout << pwd << endl;
			//MessageBox(NULL, pwd, pwd, 0);
		}
		if (vec[0] == "cd")
		{
			if (vec.size() > 1)
			{
				if (!SetCurrentDirectory(vec[1].c_str()))
				{
					std::cerr << "Error setting current directory: #" << GetLastError();
					return 1;
				}
			}
			else
			{
				cout << "Error" << endl;
			}
		}
		if (vec[0] == "create")
		{
			if (vec.size() > 1)
			{
				CreateFile(vec[1].c_str(),  // name of file
					GENERIC_WRITE,          // open for writing
					0,                      // do not share
					NULL,                   // default security
					CREATE_NEW,             // create new file only
					FILE_ATTRIBUTE_NORMAL,  // normal file
					NULL);
			}
			else
			{
				cout << "Error" << endl;
			}
		}
		if (vec[0] == "ls")
		{
			if (vec.size() == 1)
			{
				WIN32_FIND_DATA ffd;
				LARGE_INTEGER filesize;
				TCHAR szDir[MAX_PATH];
				size_t length_of_arg;
				HANDLE hFind = INVALID_HANDLE_VALUE;
				DWORD dwError = 0;
				if (!GetCurrentDirectory(MAX_PATH, pwd))
				{
					std::cerr << "Error getting current directory: #" << GetLastError();
					return 1; // quit if it failed
				}
				StringCchCopy(szDir, MAX_PATH, pwd);
				StringCchCat(szDir, MAX_PATH, TEXT("\\*"));
				hFind = FindFirstFile(szDir, &ffd);
				if (INVALID_HANDLE_VALUE == hFind)
				{
					cout << (TEXT("FindFirstFile")) << endl;
					return dwError;
				}
				do
				{
					if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
					}
					else
					{
						filesize.LowPart = ffd.nFileSizeLow;
						filesize.HighPart = ffd.nFileSizeHigh;
						_tprintf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);
					}
				}
				while (FindNextFile(hFind, &ffd) != 0);

				dwError = GetLastError();
				if (dwError != ERROR_NO_MORE_FILES)
				{
					cout << TEXT("FindFirstFile") << endl;
				}

				FindClose(hFind);
			}
			else
			{
				cout << "Error" << endl;
			}
		}
		if (vec[0] == "secret")
		{
			if (vec.size() == 1)
			{
				if (!SetCurrentDirectory("C:\\Users\\magshimim\\Desktop\\windows_shell\\windows_shell"))
				{
					std::cerr << "Error setting current directory: #" << GetLastError();
					return 1;
				}
				HMODULE dll = LoadLibrary("C:\\Users\\magshimim\\Desktop\\windows_shell\\windows_shell\\Secret.dll");
				if (NULL == dll)
				{
					cout << "cnat load this dll file!" << endl;
				}
				AvVersion func = (AvVersion)GetProcAddress(dll, "TheAnswerToLifeTheUniverseAndEverything");
				if (func == NULL)
				{
					cout << "load _version function error!" << endl;
				}
				unsigned int answer = func();
				cout << "the answer is: " << answer << endl;
			}
			else
			{
				cout << "Error!!!" << endl;
			}
		}

	}

	system("Pause");
	return 0;
}